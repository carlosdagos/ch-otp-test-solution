# CH/OTP Test solution

Please find in this repository the solution to your task.

## Overview

A node is started such as:

`stack exec 127.0.0.1 8000 -- --with-seed 1`

The first message printed will be that it's waiting for the rest of the nodes. The file in the repository `config.yaml`, has a list of nodes that make up the entirety of the nodes in the application. Each node uses this information to know which nodes should be online before it starts sending messages. These messages are printed to `stderr` so by redirecting the error output to `/dev/null` the program will just wait until all nodes are online and finally just print the result to `stdout`.

An example of how I ran the program can be found here: http://imgur.com/a/2I9w8

I've also added a file `run.sh` which will run it in the same terminal session, though that normally produces garbled text that's difficult to read.

## Main program

Along the file `src/Main.hs` I've commented the program trying to keep it brief but expressive.

Unfortunately, in line `104` you can see I have a `FIXME`, since I was unable to refresh the online peers. I was expecting the function to return the list of connected peers, but when a peer goes offline the function keeps returning it. I tried adding a handler for `sigint` and `sigterm` on each node, executing a `closeLocalNode` when such signals were received, but that didn't work either.

I'm still looking at the tutorials and documentation on Cloud Haskell to get this working properly.

## Notes

As you can see I'm using some unix-specific libraries and functions, so this will not run on Windows.

All in all I took some days of the week experimenting with different snippets and trying to put it all together. While it's not perfect, I hope you can take some time to review it and provide me with the feedback, I would greately appreciate it.

Carlos.
