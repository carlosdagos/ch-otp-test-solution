import           Control.Concurrent.MVar
import           Data.Time.Exts.Unix
import           Lib
import           Test.Tasty
import           Test.Tasty.HUnit

message1 :: ProcMessage
message1 = ProcMessage (UnixDateTimeMillis 1) 1.0 "127.0.0.1:8000"

message2 :: ProcMessage
message2 = ProcMessage (UnixDateTimeMillis 2) 1.0 "127.0.0.1:8000"

main :: IO ()
main = defaultMain messageTests

messageTests :: TestTree
messageTests = testGroup "Message tests"
  [ testCase "Messages are ordered"           ordMessages
  , testCase "Message product sum is correct" calcMessages
  , testCase "Message product sum is correct (same message)" calcMessages'
  , testCase "Message product sum is correct (same message bis)" calcMessages''
  , testCase "No messages are lost"           addMessages
  ]

ordMessages :: Assertion
ordMessages = message1 < message2 @?= True

calcMessages :: Assertion
calcMessages
  = getMessagesProducts [message1, message2] @?= 3.0

calcMessages' :: Assertion
calcMessages'
  = getMessagesProducts [message1, message1, message2] @?= 6.0

calcMessages'' :: Assertion
calcMessages''
  = getMessagesProducts [message1, message1, message2, message2] @?= 10.0

addMessages :: Assertion
addMessages = do
  msgs     <- emptyMessageList

  addMessage msgs message1
  addMessage msgs message2
  addMessage msgs message1

  msgsList <- readMVar msgs
  msgsList @?= [message1, message1, message2]
